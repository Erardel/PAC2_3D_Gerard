# PAC2 3D

En aquesta segona PAC crearem un nivell d'un FPS(First Person Shooter) on l'objectiu serà arribar al final del nivell.

## Com es juga?

El menú es controla amb el ratolí, per moure al personatge s'utilitzen els controls clàssics de WASD, per canviar d'arma i recaregar s'utilitzen la 'Q' i la 'R' respectivament, per saltar s'utilitza 'Space', per còrrer 'LeftShift' i per apuntar i disparar els botons dret i esquerra del ratolí.

## Parts Implementades
### Escenari
![image info](images/Escenari.PNG)

L'escenari que he creat és un bosc gran amb enemics, un petit edifici amb enemics normals i un enemic més fort que al matar-lo solta la clau que necessitem per accedir a l'edifici gran.
Per crear l'escenari he utilitzat l'eina de Unity Terrain, per poder crear un bosc dens amb petits turons envoltat de muntanyes. També he utilitzat assets de 3r com les textures, edificis petits, caixes, ... per emplenar l'escenari.
### Armes
![image info](images/Pistol.PNG)
![image info](images/AK.PNG)

Per aquesta pràctica he implementat dos tipus d'armes: una pistola, amb una cadència menys elevada però amb més precisió; i una ametralladora lleugera, que té més cadència pero a costa de precisió a part de ser automàtica. El jugador pot canviar de arma en tot moment.
Les bales són petits GameObjects instanciats a la boca del canó de les armes que les hi dono molta velocitat, i si colisionen amb algun collider i es un enemic o el jugador els fa mal depenent si colisionen al cap o al cos.
![image info](images/WeaponScript.PNG)

Per fer els 2 tipus de armes he usat nomès un script on es pot modificar diversos paràmetres per crear diferents tipus de armes:
-Shooting Delay: temps que passa entre bala i bala
-Bullets per Burst: quantitat de bales que es disparen si l'arma esta en mode BURST
-Burst B Left: quantes bales falten per acabar el burst
-Spread Intensity: Intensitat en la qual les bales no van cap al centre
-Bullet Prefab / BulletSpawn: Prefab de la bala i posició d'on surt
-Bullet Vel: velocitat de la bala
-Bullet Life Time: temps de vida de la bala
-Muzzle Effect: particula que simula el flash de l'arma al disparar
-CurrentShooting: Mode de l'arma: AUTO, SINGLE, BURST
-ReloadTime: Temps Que tarda en recarregar l'arma
-Magazine Size: Total de bales per carregador
-Bullets Left: bales totals del carregador actual
-Total Magazines: Numero de carregadors que tens al principi
-Ammo display / Total bullets display: text dde la ui on es mostra les dades de bales
-Weapon Controller: Script on gestionem el canvi de armes del jugador
### Vida i escut
![image info](images/PlayerHealthScript.PNG)


El jugador disposa tant de uns punts de vida com de salut. Si té escut, al rebre una bala, el mal es distribueix majoritariament a l'escut, si no ens queda punts d'escut tot el mal va a parar als punts de salut.
Aquest script té les funcions necessaries per distribuïr el mal entre l'escut i la vida i per actualitzar eh HUD 
### UI vida i escut
Al HUD del videojoc, tenim 2 barres, una blava de escut i una roja de vida. Cada cop que et fan mal o et cures, s'actualitza la barra per a que el jugador ho pugui veure millor visualment.
### Plataformes mòbils
![image info](images/MobilePlatforms.PNG)

Per l'escenari, sobretot en en nivell interior, hi han plataformes mòbils que ens permeten desplaçar-nos per l'escenari per accedir a posicions que no podriem de manera normal.
He creat un prefab de la plataforma mòbil on podem ficar un array de posicions i la plataforma anirà seguint el ordre del array, amb una velocitat determinada.

### Portes Tancades
![image info](images/DoorScript.PNG)

Per accedir a la zona interior es necessari obrir una porta que nomès és pot obrir si obtenim la clau d'un enemic més fort.
Al script tenim la opció de no necesitar cap clau per obrir la porta. Per obrir la porta hi ha un collider en mode trigger als peus de la porta on si el jugador hi és s'activa l'animació d'obrir la porta, que es tanca uns segons desprès. Al script comprovem si el player te la clau per poder obrir la porta accedint al script InventoryController del Player
### Enemics
![image info](images/EnemyController.PNG)

Per l'escenari hi ha molts enemics que si ens veuen ens perseguiran i si estan aprop ens dispararan.
L'enemic te 3 estats: Patrol, Chase i Attack. En l'estat de patrol l'enemic seleccionarà un punt que estigui dins de la navMesh i caminarà cap allà, si arriba al punt o ha passat un cert temps selecciona un altre punt.
En l'estat de Chase, l'enemic anirà cap a la direcció del player i en l'estat de Attack, l'enemic es quedarà quiet i dispararà al player.
Com al Player, es pot modificar la cadència de dispar de l'enemic, així com els rangs de Chase i Attack. Per la part frontal de l'enemic, he creat un con de visió, que té més rang que el cercle de Chase, simulant la visió de l'enemic. L'enemic que te la clau per obrir la porta té molt més rang de visió i d'atac que els enemics normals.
### Items de vida, escut i munició
![image info](images/Items.PNG)

Hi ha repartits per l'escenari 3 tipus d'items que ens ajudaran a superar el nivell: munició per la nostra arma activa, vida i salut. Els enemics al morir dexen caure un d'aquests 3 items. Hew creat un script que els 3 items comparteixen que fa que rotin sobre el seu eix, per poder identificar-los de manera més senzilla.
He creat un tag per cada tipus de item per poder saber quin és el que agafem al passar per sobre.
### Pantalla Game Over
Al morir o al superar el nivell, tornem al principi per si volem tornar a intentar-ho o marxar del joc.
Per crear aixó, he deshabilitat els controls i la opció de disparar si la UI amb els botons de Start i Exit està activa.
## Altres Implementacions

### Moviment del personatge
![image info](images/PlayerController.PNG)

Amb aquest script i el component de Character Controller, podem canviar la velocitat, la gravetat i la potencia de salt mitjançant el inspector. Dins del script rebem els inputs horitzontals i verticals per poder desplaçar-nos.

### Moviment de Càmera
![image info](images/CameraMovement.PNG)

El moviment de la càmera l'he aconseguit agafant els inputs horitzontals i verticals del mouse, havent bloquejat el ratolí al centre de la pantalla i havent-lo amagat, per rotar la càmera, amb un tope a l'eix x. També he sincronitzat la rotació Y amb la del cos del jugador ja que així al apretar la telca per avançar ens desplaçarem sempre cap a on apuntem.

### 
## Assets externs
Assets:
Low Poly FPS Map Lite - https://assetstore.unity.com/packages/3d/environments/low-poly-fps-map-lite-258453
Low-Poly Simple Nature Pack - https://assetstore.unity.com/packages/3d/environments/landscapes/low-poly-simple-nature-pack-162153
Low Poly Weapons VOL.1 - https://assetstore.unity.com/packages/3d/props/guns/low-poly-weapons-vol-1-151980
Stylized Grass Texture - https://assetstore.unity.com/packages/2d/textures-materials/glass/stylized-grass-texture-153153
## Links

https://vimeo.com/944997874?share=copy

https://gitlab.com/Erardel/PAC2_3D_Gerard/-/releases/PAC2