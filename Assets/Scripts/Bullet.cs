using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private int dmg;
    private void OnCollisionEnter(Collision collision)
    {
        //print("hit " + collision.gameObject.name);
        
        if (collision.gameObject.name.Equals("Terrain"))
        {
            Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
            print(collision.gameObject.name);
            if (collision.gameObject.name.Equals("Body"))
            {
                dmg = 20;
            }
            else if (collision.gameObject.name.Equals("Head")){
                dmg = 50;
            }
            if (collision.gameObject.name.Contains("Enemy_Rifle"))
            {
                EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();
                if (enemy != null)
                    enemy.Damage(dmg);
            }
            else
            {
                EnemyController enemy = collision.gameObject.transform.parent.GetComponent<EnemyController>();
                if(enemy != null)
                enemy.Damage(dmg);
            }
        } 
        else if (collision.gameObject.transform.parent != null)
        {
            if (collision.gameObject.transform.parent.CompareTag("Player"))
            {
                if (collision.gameObject.name.Equals("Body"))
                {
                    dmg = 10;
                }
                else if (collision.gameObject.name.Equals("Head"))
                {
                    dmg = 20;
                }
                PlayerHealth pHealth = collision.gameObject.transform.parent.GetComponent<PlayerHealth>();
                pHealth.Damage(dmg);
            }
            
            
        }
        
        Destroy(gameObject);

    }
}
