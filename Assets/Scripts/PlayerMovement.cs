using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Weapon;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;

    public float spd = 6f;
    public float grav = -9.81f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;

    bool isGrounded;
    bool isMoving;

    bool blockControls = true;
    private Vector3 lastPosition = new Vector3(0f, 0f, 0f);

    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        if(blockControls)
        {
            return;
        }
        //Comprobar si esta al terra
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y <0f )
        {
            velocity.y = -2f;
        }

        //Imputs de moviment
        float x = Input.GetAxis( "Horizontal" );
        float z = Input.GetAxis( "Vertical" );
        spd = 6f;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            spd = 12f;
        }
        

        Vector3 move = transform.right * x + transform.forward * z;

        //Moure el player
        controller.Move( move * spd * Time.deltaTime );

        if(Input.GetButtonDown("Jump") && isGrounded )
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * grav);
        }

        velocity.y += grav * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        if (lastPosition != gameObject.transform.position && isGrounded)
        {
            isMoving = true;
        } else
        {
            isMoving= false;
        }
        lastPosition = gameObject.transform.position;
    }

    public void SetBlockControls(bool controls)
    {
        blockControls = controls;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name.Equals("Exit"))
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
