using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.GraphicsBuffer;
using Random = UnityEngine.Random;

public class EnemyController : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    public LayerMask layerGround, layerPlayer;

    //Patrol
    public Vector3 walkPoint;
    private bool walkPointSet;
    public float walkPointRange;
    private float timeWalking;

    //Attack
    public float timeBetweenAttacks;
    private bool alreadyAttacked;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public float spreadIntensity;

    //State
    public float sightRange, attackRange;
    public bool canSeePlayer, canAttackPlayer;


    //Health
    private float health = 100f;
    public GameObject ammoPrefab;
    public GameObject healthPrefab;
    public GameObject shieldPrefab;
    public GameObject card;
    public bool hasCard;

    //RangeSight
    public float coneAngle = 60f; // Angle of the cone
    public float coneDistance = 40f; // Maximum distance of the cone

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    private void FixedUpdate()
    {
        
        canSeePlayer = Physics.CheckSphere(transform.position, sightRange, layerPlayer);
        canAttackPlayer = Physics.CheckSphere(transform.position, attackRange, layerPlayer);
        CheckCone();

        if(!canSeePlayer && !canAttackPlayer ) {
            Patrolling();
        }
        if(canSeePlayer && !canAttackPlayer ) {
            Chase();
        }
        if(canSeePlayer && canAttackPlayer ) {
            Attack();
        }
        timeWalking += Time.deltaTime;
        if(health <= 0 && !hasCard)
        {
            Vector3 spawnPos = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
            int i = Random.Range(0, 3);
            print(i);
            if(i == 0)
                Instantiate(ammoPrefab, spawnPos, Quaternion.identity);
            else if(i == 1)
                Instantiate(healthPrefab, transform.position, Quaternion.identity);
            else if(i == 2)
                Instantiate(shieldPrefab, transform.position, Quaternion.identity);

            Destroy(gameObject);
        } else if(health <= 0 && hasCard)
        {
            Vector3 spawnPos = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
            Instantiate(card, spawnPos, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    private void Patrolling()
    {
        if (!walkPointSet)
        {
            SearchWalkPoint();
        }
        if(walkPointSet)
        {
            agent.SetDestination(walkPoint);
        }

        Vector3 dist = transform.position - walkPoint;
        if(dist.magnitude < 5f)
        {
            walkPointSet = false;
        }
        if(timeWalking >= 5f)
        {
            timeWalking = 0;
            walkPointSet = false;
        }
    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);
        Vector3 pos = new Vector3(randomX + transform.position.x, 0f, randomZ + transform.position.z);
        float posY = Terrain.activeTerrain.SampleHeight(pos);
        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        if(Physics.Raycast(walkPoint, -transform.up, 100f, layerGround))
        {
            walkPointSet = true;
        }
    }

    private void Chase()
    {
        agent.SetDestination(player.position);
    }
    
    private void Attack()
    {
        agent.SetDestination(transform.position);
        transform.LookAt(player);

        if (!alreadyAttacked)
        {
            alreadyAttacked = true;
            Invoke("ResetAttack", timeBetweenAttacks);
            
            GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
            Vector3 shootDir = CalculateDir().normalized;
            bullet.transform.forward = shootDir;
            Debug.Log(shootDir);
            bullet.GetComponent<Rigidbody>().AddForce(shootDir * 100f, ForceMode.Impulse);
            StartCoroutine(DestroyBullet(bullet, 3f));
        }

    }

    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    private IEnumerator DestroyBullet(GameObject bullet, float delay)
    {

        yield return new WaitForSeconds(delay);
        Destroy(bullet);
    }

    public void Damage(float damage)
    {
        health -= damage;
        print(health);
    }

    private void CheckCone()
    {
        // Calculate cone direction based on transform forward
        Vector3 coneDirection = transform.forward;

        // Get all colliders in the cone
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, coneDistance);

        foreach (Collider collider in hitColliders)
        {

            // Check if the collider is within the cone angle
            Vector3 directionToCollider = collider.transform.position - transform.position;
            float angleToCollider = Vector3.Angle(coneDirection, directionToCollider);

            if (angleToCollider <= coneAngle * 0.5f)
            {
                // Do something with the collider within the cone
                if(collider.gameObject.transform.parent != null)
                    if(collider.gameObject.transform.parent.CompareTag("Player"))
                        canSeePlayer = true;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        // Set the gizmo color
        Gizmos.color = Color.yellow;

        // Calculate cone direction based on object's forward vector
        Vector3 coneDirection = transform.forward;

        // Calculate cone base points
        Vector3 coneBaseLeft = Quaternion.Euler(0, -coneAngle / 2, 0) * coneDirection * coneDistance;
        Vector3 coneBaseRight = Quaternion.Euler(0, coneAngle / 2, 0) * coneDirection * coneDistance;

        // Draw the cone
        Gizmos.DrawLine(transform.position, transform.position + coneBaseLeft);
        Gizmos.DrawLine(transform.position, transform.position + coneBaseRight);
        Gizmos.DrawLine(transform.position + coneBaseLeft, transform.position + coneBaseRight);

        Vector3 targetP;
        targetP = transform.forward;
        Vector3 dir = targetP - bulletSpawn.position;

        float x = UnityEngine.Random.Range(-spreadIntensity, spreadIntensity);
        float y = UnityEngine.Random.Range(-spreadIntensity, spreadIntensity);

        Gizmos.DrawLine(bulletSpawn.position, dir + new Vector3(x, y, 0));
    }

    public Vector3 CalculateDir()
    {
        
        Vector3 targetP;

        targetP = transform.forward;

        Vector3 dir = targetP - bulletSpawn.position;

        float x = UnityEngine.Random.Range(-spreadIntensity, spreadIntensity);
        float y = UnityEngine.Random.Range(-spreadIntensity, spreadIntensity);

        return targetP + new Vector3(x, y, 0);
    }
}
