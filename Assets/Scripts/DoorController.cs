using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    private Animator animator;
    public bool needCard;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player") && !needCard)
        {
            Debug.Log("Door");
            animator.SetTrigger("Open");
        } else if(other.CompareTag("Player") && needCard)
        {
            InventoryController inv = other.gameObject.GetComponent<InventoryController>();
            if (inv != null && inv.HasCard())
            {
                Debug.Log("DoorCard");
                animator.SetTrigger("Open");
                //inv.SetHasCard(false);
            }
        }
    }
}
