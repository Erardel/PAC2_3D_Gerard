using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    public Transform playerTransform;
    public float mouseSen = 500f;

    float xRot = 0f;
    float yRot = 0f;

    bool blockControls = true;
    void Start()
    {
         //Bloquejar el cursor al centre
        //transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
    }

    void Update()
    {
        if (blockControls)
        {
            return;
        }
        //Obterner imput de mouse
        float mouseX = Input.GetAxis("Mouse X") * mouseSen * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSen * Time.deltaTime;

        //Rotar mouse
        xRot -= mouseY;
        //L�mit de rotaci� de 90� per sobre i per sota
        xRot = Mathf.Clamp(xRot, -90f, 90f); 
        
        yRot += mouseX;


        //Aplicar les rotacions; el cap pot rotar cap amunt i cap avall per� el cos nom�s cap als cantons
        transform.localRotation = Quaternion.Euler(xRot, 0f, 0f);
        playerTransform.rotation = Quaternion.Euler(0f, yRot, 0f);
    }

    public void SetBlockControls(bool controls)
    {
        blockControls = controls;
    }
}
