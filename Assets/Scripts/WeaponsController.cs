using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsController : MonoBehaviour
{
    private GameObject[] weapons;
    // Start is called before the first frame update
    void Start()
    {
        weapons = GameObject.FindGameObjectsWithTag("Weapon");
        weapons[0].SetActive(false);
    }

    public void ChangeWeapon(GameObject actWeapon, Vector3 pos)
    {
        foreach(GameObject weapon in weapons)
        {
            if (weapon.name.Equals(actWeapon.name))
            {
                Debug.Log(pos);
                weapon.transform.localPosition = pos;
                weapon.transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
                weapon.SetActive(false);
            }
            else
            {
                
                weapon.SetActive(true);
                weapon.GetComponent<Weapon>().SetTotalBulletsText();
            }
        }
    }

    

    
}
