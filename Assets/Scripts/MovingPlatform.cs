using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] GameObject[] wayPoints;
    [SerializeField] GameObject platform;

    public float speed = 2f;
    private int nextIndex = 0;
    private Transform prevWaypoint;
    private Transform nextWaypoint;
    private float timeToWaypoint;
    private float elapsedTime;

    private GameObject player;
    private bool playerEntered = false;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        NextWaypoint();
        print(prevWaypoint.position);
        print(nextWaypoint.position);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        elapsedTime += Time.deltaTime;
        float percentage = elapsedTime / timeToWaypoint;
        percentage = Mathf.SmoothStep(0, 1, percentage);
        platform.transform.position = Vector3.Lerp(prevWaypoint.position, nextWaypoint.position, percentage);
        if(percentage >= 1 )
        {
            NextWaypoint();
        }
        CheckPLayer();
    }

    private void OnDrawGizmos()
    {
        Vector3 overlapBox = new Vector3(platform.transform.position.x, platform.transform.position.y + 0.5f, platform.transform.position.z);
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Vector3 size = new Vector3(platform.transform.localScale.x, platform.transform.localScale.y, platform.transform.localScale.z);
        Gizmos.DrawCube(overlapBox, size);

    }

    private void CheckPLayer()
    {
        int playercheck = 0;
        Vector3 overlapBox = new Vector3(platform.transform.position.x, platform.transform.position.y + 0.5f, platform.transform.position.z);
        Vector3 size = new Vector3(platform.transform.localScale.x /2 , platform.transform.localScale.y / 2, platform.transform.localScale.z / 2);
        Collider[] colls = Physics.OverlapBox(overlapBox, size);
        foreach (Collider coll in colls)
        {
            if (coll.gameObject.CompareTag("Player"))
            {
                playercheck++;
            }
        }
        if (playercheck == 0 && !playerEntered)
        {
            player.transform.SetParent(null);
            playerEntered = true;
        } else if(playercheck >= 1 && playerEntered)
        {
            playerEntered = false;
            player.transform.SetParent(platform.transform);
        }
    }

    private void NextWaypoint()
    {
        prevWaypoint = wayPoints[nextIndex].transform;
        nextIndex++;
        if(nextIndex >= wayPoints.Length)
        {
            nextIndex = 0;
        }
        nextWaypoint = wayPoints[nextIndex].transform;
        elapsedTime = 0;
        float distance = Vector3.Distance(prevWaypoint.position, nextWaypoint.position);
        timeToWaypoint = distance / speed;
    }

   

}
