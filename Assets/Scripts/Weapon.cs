using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    //Pum-Pum
    public bool isShooting, canShoot;
    bool allowReset = true;
    public float shootingDelay = 0.1f;

    //Burst
    public int bulletsPerBurst = 3;
    public int burstBLeft;

    //Spread
    public float spreadIntensity = 1f;

    //Bullet
    public GameObject bulletPrefav;
    public Transform bulletSpawn;
    public float bulletVel = 100f;
    public float bulletLifeTime = 3f;
    
    public GameObject muzzleEfect;

    public enum ShootingMode {Single, Burst, Auto};
    private Animator animator;

    public ShootingMode currentShootingMode = ShootingMode.Auto;

    //Reload
    public float reloadTime;
    public int magazineSize, bulletsLeft, totalMagazines;
    public bool isReloading;
    private int totalBullets;
    //UI
    public TextMeshProUGUI ammoDisplay;
    public TextMeshProUGUI totalBulletsDisplay;

    private Camera cam;

    public bool isZooming;

    public WeaponsController weaponController;
    private Vector3 firstPos = Vector3.zero;
    private void Awake()
    {
        firstPos = transform.localPosition;
        canShoot = true;
        burstBLeft = bulletsPerBurst;
        bulletsLeft = magazineSize;
        animator = GetComponent<Animator>();
        cam = Camera.main;
        weaponController = transform.parent.transform.parent.GetComponent<WeaponsController>();
        totalBullets = magazineSize * totalMagazines;
        totalBulletsDisplay.text = (totalBullets - bulletsLeft).ToString();

    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            StartCoroutine(ChangeWeapon(gameObject));
        }
        
        if (currentShootingMode == ShootingMode.Auto && ammoDisplay.IsActive())
        {
            isShooting = Input.GetKey(KeyCode.Mouse0);
        } else if ((currentShootingMode == ShootingMode.Single || currentShootingMode == ShootingMode.Burst) && ammoDisplay.IsActive())
        {
            isShooting = Input.GetKeyDown(KeyCode.Mouse0);
        }
        //Mirar si apretem el bot� de disparar
        if (canShoot && isShooting && bulletsLeft > 0)
        {
            burstBLeft = bulletsPerBurst;
            if(totalBullets > 0)
            {
                FireWeapon();
            }
            
        }

        if((Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !isReloading && totalBullets > 0) || (canShoot && !isShooting && !isReloading && bulletsLeft <=0 && totalBullets > 0))
        {
            ReloadWeapon();
        }

        if(ammoDisplay!= null)
        {
            ammoDisplay.text = bulletsLeft + "/" + magazineSize;
        }
        isZooming = Input.GetKey(KeyCode.Mouse1);
        ZoomWeapon();

    }

    private void FireWeapon()
    {
        animator.SetTrigger("Recoil");
        muzzleEfect.GetComponent<ParticleSystem>().Play();
        bulletsLeft--;
        totalBullets--;
        canShoot = false;

        Vector3 shootDir = CalculateDir().normalized;
        GameObject bullet = Instantiate(bulletPrefav, bulletSpawn.position, Quaternion.identity);
        bullet.transform.forward = shootDir; // To Enemy Controller

        bullet.GetComponent<Rigidbody>().AddForce(shootDir * bulletVel, ForceMode.Impulse);
        StartCoroutine(DestroyBullet(bullet, bulletLifeTime));
        
        if(allowReset)
        {
            Invoke("ResetShot", shootingDelay);
            allowReset = false;
        }

        //Burst
        if(currentShootingMode == ShootingMode.Burst && burstBLeft > 1)
        {
            burstBLeft--;
            Invoke("FireWeapon", shootingDelay);
        }

    }

    private void ReloadWeapon()
    {
        animator.SetTrigger("Reload");
        isReloading = true;
        canShoot = false;
        Invoke("ReloadComplete", reloadTime);

    }

    private void ReloadComplete()
    {
        if(totalBullets < magazineSize)
        {
            bulletsLeft = totalBullets;
        } else
        {
            bulletsLeft = magazineSize;
        }
        totalBulletsDisplay.text = (totalBullets - bulletsLeft).ToString();
        isReloading = false;
        canShoot = true;
    }

    public Vector3 CalculateDir()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

        Vector3 targetP;
        
        targetP = ray.GetPoint(100);

        Vector3 dir = targetP - bulletSpawn.position;

        float x = UnityEngine.Random.Range(-spreadIntensity, spreadIntensity);
        float y = UnityEngine.Random.Range(-spreadIntensity, spreadIntensity);

        return dir + new Vector3(x, y, 0);
    }

    private void ResetShot()
    {
        canShoot = true;
        allowReset = true;
    }

    private void ZoomWeapon()
    {
        if(isZooming)
        {
            cam.fieldOfView = 30f;
            spreadIntensity = 0f;
        }
        else
        {
            cam.fieldOfView = 70f;
            spreadIntensity = 1f;
        }
    }

    private IEnumerator ChangeWeapon(GameObject actualWeapon)
    {
        animator.SetTrigger("Change");
        yield return new WaitForSeconds(1f);
        weaponController.ChangeWeapon(actualWeapon, firstPos);
    }

    private IEnumerator DestroyBullet( GameObject bullet, float delay)
    {

        yield return new WaitForSeconds(delay);
        Destroy(bullet);
    }
    
    public void SetTotalBulletsText()
    {
        totalBulletsDisplay.text = (totalBullets - bulletsLeft).ToString();
    }

    public void PickAmmo()
    {
        totalBullets += magazineSize;
        totalBulletsDisplay.text = (totalBullets - bulletsLeft).ToString();
    }
}
