using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    // Start is called before the first frame update
    public Slider healthSlider;
    public Slider shieldSlider;

    private float healthValue = 100;
    private float shieldValue = 100;

    public int hpRestored = 30;
    public int shieldRestored = 30;
    void Start()
    {
        healthSlider.value = healthValue;
        shieldSlider.value = shieldValue;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStats();
        if(healthValue <=0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void Damage(float dmg)
    {
        if(shieldValue > 0)
        {
            shieldValue -= (dmg/4) * 3;
            if(shieldValue < 0)
            {
                shieldValue = 0;
            }
            healthValue -= (dmg / 4);
        }
        else
        {
            healthValue -= dmg;
        }
    }

    private void UpdateStats()
    {
        healthSlider.value = healthValue;
        shieldSlider.value = shieldValue;
    }

    public void SetHealth()
    {
        healthValue += hpRestored;
        if(healthValue > 100)
            healthValue = 100;
    }
    public void SetShield()
    {
        shieldValue += shieldRestored;
        if (shieldValue > 100)
            shieldValue = 100;
    }

}
