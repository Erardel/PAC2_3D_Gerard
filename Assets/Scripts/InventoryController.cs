using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    private bool hasCard = false;
    private Weapon weapon;
    private PlayerHealth healthCtrl;
    // Start is called before the first frame update
    void Start()
    {
        healthCtrl = GetComponent<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Card"))
        {
            print("Got Card");
            hasCard = true;

            Destroy(other.gameObject);
        } else if (other.CompareTag("Ammo"))
        {
            weapon = GameObject.FindGameObjectWithTag("Weapon").GetComponent<Weapon>();
            weapon.PickAmmo();
            Destroy(other.gameObject);
        } else if (other.CompareTag("HP"))
        {
            Debug.Log("Hp picked");
            healthCtrl.SetHealth();
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Shield"))
        {
            Debug.Log("Shield picked");
            healthCtrl.SetShield();
            Destroy(other.gameObject);
        }
    }

    public void SetHasCard(bool newHasCard)
    {
        this.hasCard = newHasCard;
    }

    public bool HasCard()
    {
        return hasCard;
    }
}
