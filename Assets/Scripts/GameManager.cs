using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject menuUI;
    public GameObject gameplayUI;
    public GameObject player;
    private MouseMovement mouseScript;
    public KeyCode[] blockedKeys;
    private bool gameStarted = false;
    // Start is called before the first frame update
    void Start()
    {
        mouseScript = GameObject.Find("HeadPlayer").GetComponent<MouseMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameStarted)
        {
            foreach (KeyCode key in blockedKeys)
            {
                // Check if the blocked key is pressed
                if (Input.GetKeyDown(key))
                {
                    // Do nothing or handle the blocked key press as desired
                    //Debug.Log("Blocked key pressed: " + key);
                    // You can add your logic here, like showing a message or ignoring the key press
                }
            }
        }
    }

    public void StartGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        player.GetComponent<PlayerMovement>().SetBlockControls(false);
        mouseScript.SetBlockControls(false);
        menuUI.SetActive(false);
        gameplayUI.SetActive(true);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
